// return an array containing the variables
export function getVariable(data)
{
    // var data = require('../ressources/JSON/file.json');
    var length = data.Variables.variable.length;
    var Immutable = require('immutable');
    variables = Immutable.List();

    for (i = 0; i < length; i++) {
        variables = variables.push({varID:data.Variables.variable[i].varID,
                  varName:data.Variables.variable[i].varName,
                  varValue:data.Variables.variable[i].varDefaultValue});
    }
    return variables;
}

// return the index of a node verifying the nodeID
export function getIndex(nodeID, data) {
    var length = data.node.length;
    // console.info("nodeID to check: " + nodeID + " on " + length);

    for (i = 0; i < length; i++)
    if (data.node[i].nodeID == nodeID)
        return i;

    console.warn("Error in getIndex, nodeID not found: " + nodeID);
    return -2;
}

// return a string containing the question
export function getQuestion(nodeID) {
    var data = require('../ressources/JSON/file.json');
    return data.node[getIndex(nodeID, data)].questions;
}

// return an array containing the answers
export function getAnswers(nodeID) {
    var data = require('../ressources/JSON/file.json');
    nodeID = getIndex(nodeID, data);
    var length = data.node[nodeID].completions.completion.length;
    var answers = [];

    for (i = 0; i < length; i++) {
        answers.push(data.node[nodeID].completions.completion[i].answer + "\n");
    }
    return answers;
}

//------------------------------------------------------------------------------
//                      getting next nodeID
//------------------------------------------------------------------------------
// step 1: run doCondition on conditionID incrementing, once true, continue step 2
// step 2: run setProduct and setVariable with the newly found conditionID
// step 3: run getNextNode and get next node

// only called once setVariable and setProduct are done and set
// return the next index
export function getNextNode(data, nodeID, answerID, conditionID) {
    // console.info(nodeID + " " + answerID + " " + conditionID);
    // console.info(getIndex(data.node[nodeID]
    //     .completions.completion[answerID]
    //     .condition[conditionID].redirection, data));
    try {
        return getIndex(data.node[nodeID]
            .completions.completion[answerID]
            .condition[conditionID].redirection, data);
    } catch (e) {
        console.warn("Error in getNextNode: coundt find next node: " + e);
        return -2;
    }

}

export function setProduct(data, nodeID, answerID, conditionID, productList) {
}

export function setVariable(data, nodeID, answerID, conditionID, variableList) {
}

// check the condition for nodeID, answerID and conditionID using the list of variables
export function doCondition(data, nodeID, answerID, conditionID, variablesList) {
    //canno't do eval in case of hacking
    var res = true;

    // var data = require('../ressources/JSON/file.json');

    // var Immutable = require('immutable');
    // variable3 = Immutable.List();
    // variable3 = variable3.push({ varID: 0, varName: 'greyLength', varValue: 35 });
    // variable3 = variable3.push({ varID: 1, varName: 'saturation', varValue: 0 });
    // variable3 = variable3.push({ varID: 2, varName: 'wanted', varValue: 0 });

    for (logiqueID = 0; logiqueID < data.node[nodeID]
        .completions.completion[answerID]
        .condition[conditionID].logiques.length; logiqueID++) {

        str = data.node[nodeID].completions.completion[answerID]
        .condition[conditionID].logiques[logiqueID].logique;
        strArray = str.split(" ");
        if (strArray.length != 3) {
            console.log("Error in doCondition, str count " + str.length);
            return false;
        }
        console.info("in doCondition: " + str);
        //test 1: get index for variable 1
        var count = -1;
        for (i = 0; i < variablesList.size; i++) {
            if (strArray[0] == variablesList.get(i).varName) {
                count = i;
            }
        }
        if (count == -1) {
            console.log("Error in doCondition, cannot find: " + strArray[0]);
            return false;
        }

        //step 2: check variable 2
        var var2 = parseInt(strArray[2]);
        if (isNaN(var2)) {
            console.log("Error in doCondition, Not a Number: ");
            return false;
        }

        //step 3: do condition
        switch (strArray[1]) {
            case '==':
                res &= (variablesList.get(count).varValue == var2);
                break;
            case '<=':
                res &= variablesList.get(count).varValue <= var2;
                break;
            case '>=':
                res &= variablesList.get(count).varValue >= var2;
                break;
            case '<':
                res &= variablesList.get(count).varValue < var2;
                break;
            case '>':
                res &= variablesList.get(count).varValue > var2;
                break;
            default:
                console.log("Error in doCondition, not a logique operator: " + strArray[1]);
                return false;
        }
    }
    console.info("returning res at " + res);
    return res;
}
