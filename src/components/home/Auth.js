import React from 'react';
import {
  AppRegistry,
  Image,
  ListView,
  TouchableHighlight,
  StyleSheet,
  RecyclerViewBackedScrollView,
  TouchableOpacity,
  AlertIOS,
  ActivityIndicatorIOS,
  Text,
  View,
  ScrollView,
  UIExplorerBlock,
  TextInput
} from 'react-native';

import ModalPicker from 'react-native-modal-picker'
import Button from "react-native-button";
import FormButton from '../../components/mics/FormButton';

import * as globalActions from '../../reducers/global/globalActions';
import * as formationActions from '../../reducers/formation/formationActions';
import * as deductionActions from '../../reducers/deduction/deductionActions';
import * as profileActions from '../../reducers/profile/profileActions';

import { Map } from 'immutable';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import { Actions } from 'react-native-router-flux';

import NavigationBar from 'react-native-navbar';

const actions = [ globalActions, formationActions, deductionActions, profileActions ];

const t = require('tcomb-form-native');
let Form = t.form.Form;

import Dimensions from 'Dimensions';
var {height, width} = Dimensions.get('window');

function mapStateToProps(state) {
  return {
    ...state
  };
};

function mapDispatchToProps(dispatch) {

  const creators = Map()
  .merge(...actions)
  .filter(value => typeof value === 'function')
  .toObject();

  return {
    actions: bindActionCreators(creators, dispatch),
    dispatch
  };
}

class Auth extends React.Component {
  constructor(props) {
    super(props);
    // this.errorAlert = new ErrorAlert();
    this.state = {
			pickerData: [
            { key: 0, label: 'Cherries' },
            { key: 1, label: 'Cranberries' },
            { key: 2, label: 'Pink Grapefruit' },
            { key: 3, label: 'Raspberries' }],
            value: {
                name: this.props.profile.form.name,
                surname: this.props.profile.form.surname,
                email: this.props.profile.form.fields.email,
                isAPro: this.props.profile.form.fields.isAPro,
                adress: this.props.profile.form.adress,
                profession: this.props.profile.form.profession
              },
      textInputValue: ''
		};
    // this.props.profile.form.isFetching = false;
  }

  componentWillReceiveProps(nextprops) {
    this.setState({
      value: {
        name: nextprops.profile.form.name,
        surname: nextprops.profile.form.surname,
        email: nextprops.profile.form.fields.email,
        isAPro: nextprops.profile.form.fields.isAPro,
        adress: nextprops.profile.form.adress,
        profession: nextprops.profile.form.profession
      }
    });
  }
  onButtonPress(value) {
    console.info('In onButtonPress');
    console.info(this.state);
  }
  onChange(value) {
    // if (value.username != '') {
    //   this.props.actions.onAuthFormFieldChange('username',value.username);
    // }
    // if (value.email != '') {
    //   this.props.actions.onAuthFormFieldChange('email',value.email);
    // }
    // if (value.password != '') {
    //   this.props.actions.onAuthFormFieldChange('password',value.password);
    // }
    // if (value.passwordAgain != '') {
    //   this.props.actions.onAuthFormFieldChange('passwordAgain',value.passwordAgain);
    // }
    this.setState(
      {value}
    );
    console.info('In on change');
    // console.info(value);
  }

  isapro(boo)
  {
    let select =
      <View>
        <TouchableOpacity style={{marginTop: 10}} >
          <Text>Selection de la profession</Text>
        </TouchableOpacity>
        <ModalPicker
          data={this.state.pickerData}
          initValue="Select something yummy!"
          onChange={(option)=>{ this.setState({value:{profession:option.label}})}}>
          <TextInput
            style={{borderWidth:1, borderColor:'#ccc', padding:10, height:30, marginBottom: 20}}
            editable={false}
            placeholder="Select something yummy!"
            value={this.state.value.profession} />
        </ModalPicker>
      </View>;
    if (boo)
      return select;
  }


  render(){
      // this.errorAlert.checkError(this.props.profile.form.error);
      let options = {
        auto: 'labels',
        fields: {
        }
      };

      let name = {
        label: 'Nom',
        maxLength: 20,
        editable: !this.props.profile.form.isFetching,
        hasError: this.props.profile.form.fields.usernameHasError,
        error: 'Doit avoir entre 3 et 20 characters'
      };
      let surname = {
        label: 'surname',
        maxLength: 20,
        editable: !this.props.profile.form.isFetching,
        hasError: this.props.profile.form.fields.usernameHasError,
        error: 'Doit avoir entre 3 et 20 characters'
      };
      let email = {
        label: 'Email',
        keyboardType: 'email-address',
        editable: !this.props.profile.form.isFetching,
        hasError: this.props.profile.form.fields.emailHasError,
        error: 'Doit etre une adresse email valide'
      };
      let isAPro = {
        label: 'Etes vous un professionel?',
        editable: !this.props.profile.form.isFetching,
      };

      let loginForm = t.struct({
        name: t.String,
        surname: t.String,
        email: t.String,
        isAPro: t.Boolean
      });

      options.fields['name'] = name;
      options.fields['surname'] = surname;
      options.fields['email'] = email;
      options.fields['isAPro'] = isAPro;

      let selection = this.isapro(this.state.value.isAPro);

      return (
        <View style={styles.container}>
          <ScrollView horizontal={false} width={width} height={height}>
            <Form ref="form"
              type={loginForm}
              options={options}
              value={this.state.value}
              onChange={this.onChange.bind(this)}
            />
            {selection}
            <Text>Je confirme sur lhonneur que toutes ces informations sont correcte </Text>
            <Text>Jaccepte que des donnes soit envoyer</Text>
            <FormButton
              isDisabled={!this.props.profile.form.isValid || this.props.profile.form.isFetching}
              buttonText={'Valider'}
              onPress={this.onButtonPress.bind(this)}/>
          </ScrollView>
        </View>
      );
  }

}
var styles = StyleSheet.create({
  container: {
    marginTop: 20,
    // backgroundColor: '#333333',
    flexDirection: 'column',
    flex: 1
  },
  summary: {
    fontFamily: 'BodoniSvtyTwoITCTT-Book',
    fontSize: 18,
    fontWeight: 'bold'
  },
  button: {
    backgroundColor: '#FF3366',
    borderColor:  '#FF3366',
    marginLeft: 10,
    marginRight: 10
  }
});
export default connect(mapStateToProps, mapDispatchToProps)(Auth);
