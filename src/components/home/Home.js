import React from 'react';
import { View, Text, StyleSheet } from "react-native";
import Button from "react-native-button";

import * as globalActions from '../../reducers/global/globalActions';
import * as testActions from '../../reducers/test/testActions';
import * as formationActions from '../../reducers/formation/formationActions';
import * as deductionActions from '../../reducers/deduction/deductionActions';

import { Map } from 'immutable';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { getIndex, getVars, doCondition } from '../../modules/engine'

const actions = [ globalActions, formationActions, deductionActions ];
import GoogleAnalytics from 'react-native-google-analytics-bridge';

function mapStateToProps(state) {
  return {
      ...state
  };
};

function mapDispatchToProps(dispatch) {

  const creators = Map()
          .merge(...actions)
          .filter(value => typeof value === 'function')
          .toObject();

  return {
    actions: bindActionCreators(creators, dispatch),
    dispatch
  };
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "transparent",
    // borderWidth: 2,
    // borderColor: 'red',
  }
});

// class Home extends React.Component {
let Home = React.createClass({

  render(){
    //testing
    // console.info(doCondition(0, 0, 1));
    GoogleAnalytics.trackScreenView('Home');

    let profileButtonText = 'Go to test pagegf';
    let onButtonPress = () => {
      this.props.actions.deductionScreen();
      // this.prop.actions.Test();
    }

    let formationPress = () => {
      this.props.actions.formationScreen();
      // this.prop.actions.Test;
    }
// <Button onPress={onButtonPress.bind(this)}>{profileButtonText}</Button>
    return (
      <View style={styles.container}>
        <Text>Home page</Text>
        <Button onPress={onButtonPress.bind(this)}>{profileButtonText}</Button>
        <Button onPress={formationPress.bind(this)}>formation</Button>
      </View>
    );
  }

// }
});
export default connect(mapStateToProps, mapDispatchToProps)(Home);
// module.exports = Home;
