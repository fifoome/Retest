import React from 'react';
import {
  AppRegistry,
  Image,
  ListView,
  TouchableHighlight,
  StyleSheet,
  RecyclerViewBackedScrollView,
  TouchableOpacity,
  AlertIOS,
  ActivityIndicator,
  Text,
  View
} from 'react-native';
import Button from "react-native-button";

import * as globalActions from '../../reducers/global/globalActions';
import * as formationActions from '../../reducers/formation/formationActions';
import * as deductionActions from '../../reducers/deduction/deductionActions';
const actions = [ deductionActions ];

import { Map } from 'immutable';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import { getQuestion, getAnswers } from '../../modules/engine';
import {Actions} from 'react-native-router-flux';

import GoogleAnalytics from 'react-native-google-analytics-bridge';

function mapStateToProps(state) {
  return {
    ...state
  };
};

function mapDispatchToProps(dispatch) {

  const creators = Map()
  .merge(...actions)
  .filter(value => typeof value === 'function')
  .toObject();

  return {
    actions: bindActionCreators(creators, dispatch),
    dispatch
  };
}
class Question extends React.Component {
  constructor(props) {
    super(props);
    GoogleAnalytics.trackScreenView('Question');
    GoogleAnalytics.trackEvent('testcategory', 'testaction', {label: 'v1.0.5', value: 22});
    var getRowData = (dataBlob, rowID) => {
      return dataBlob[rowID];
    }

    this.state = {
      loaded : false,
      result : false,
      dataSource : new ListView.DataSource({
        getRowData              : getRowData,
        rowHasChanged           : (row1, row2) => row1 !== row2
      })
    };
    console.info(this.props.nodeID);
    this.bindMethods();
  }
  bindMethods() {
    if (! this.bindableMethods) {
      return;
    }

    for (var methodName in this.bindableMethods) {
      this[methodName] = this.bindableMethods[methodName].bind(this);
    }
  }
  componentDidMount() {
    this.fetchData();
  }
  componentWillMount() {
    this.props.onChange(`Question #${this.props.nodeID}`);
  }
  componentWillReceiveProps(nextProps) {
    this.setState({
      loaded     : false
    });
    this.props.onChange(`Question #${nextProps.nodeID}`);
    this.setState({
      dataSource : this.state.dataSource.cloneWithRows(getAnswers(nextProps.nodeID)),
      loaded     : true
    });
  }
  shouldComponentUpdate(nextProps, nextState) {
    return true;
  }
  fetchData () {
  }

  render(){

    if (!this.state.loaded) {
      return this.renderLoadingView();
    }

    let renderRow = function (rowData, sectionID, rowID) {
        return (
          <TouchableOpacity onPress={() => Press(this.props.nodeID, rowID)}>
          <View style={styles.rowStyle}>
            <Text style={styles.rowText}> {rowData[rowID]} </Text>
          </View>
          </TouchableOpacity>
        );
    };
    renderRow = renderRow.bind(this);

    let Press = function (nodeID, answerID) {
      this.props.actions.questionScreen({nodeID:nodeID, answerID:answerID});
    };
    Press = Press.bind(this);
    console.info("Question");
    return (
      <View style={styles.container}>
        <Text style={styles.question}>Question #{this.props.nodeID}: { getQuestion(this.props.nodeID) } </Text>
        <ListView
          dataSource = {this.state.dataSource}
          style      = {styles.listview}
          renderRow  = {renderRow} />
      </View>
    );
  }
  renderLoadingView() {
    return (
      <View style={styles.header}>
      <Text style={styles.headerText}>Chargement de la question</Text>
      <View style={styles.container}>
      <ActivityIndicator
      animating={!this.state.loaded}
      style={[styles.activityIndicator, {height: 80}]}
      size="large"
      />
      </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    justifyContent: "center",
    flex: 1
  },
  activityIndicator: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  header: {
    height: 60,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#3F51B5',
    flexDirection: 'column',
    paddingTop: 25
  },
  headerText: {
    fontWeight: 'bold',
    fontSize: 20,
    color: 'white'
  },
  text: {
    color: 'white',
    paddingHorizontal: 8,
    fontSize: 16
  },
  rowStyle: {
    paddingVertical: 20,
    paddingLeft: 16,
    borderTopColor: 'white',
    borderLeftColor: 'white',
    borderRightColor: 'white',
    borderBottomColor: '#E0E0E0',
    borderWidth: 1
  },
  rowText: {
    color: '#212121',
    fontSize: 16
  },
  subText: {
    fontSize: 14,
    color: '#757575'
  },
  section: {
    flexDirection: 'column',
    justifyContent: 'center',
    // alignItems: 'flex-start',
    padding: 6,
    backgroundColor: '#2196F3'
  },
  question: {
    borderWidth: 2,
    borderColor: '#00E3E3',
    padding: 2,
    borderRadius: 5,
    fontSize: 12,
    backgroundColor: '#FFFFFF',
    color: '#474747'
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(Question);
