import React from 'react';
import {
  AppRegistry,
  Image,
  ListView,
  TouchableHighlight,
  StyleSheet,
  RecyclerViewBackedScrollView,
  TouchableOpacity,
  AlertIOS,
  ActivityIndicatorIOS,
  Text,
  View
} from 'react-native';
import Button from "react-native-button";

import * as globalActions from '../../reducers/global/globalActions';
import * as formationActions from '../../reducers/formation/formationActions';
import * as deductionActions from '../../reducers/deduction/deductionActions';

import { Map } from 'immutable';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import { getQuestion, getAnswers } from '../../modules/engine';
import {Actions} from 'react-native-router-flux';

const actions = [ globalActions, formationActions, deductionActions ];

function mapStateToProps(state) {
  return {
    ...state
  };
};

function mapDispatchToProps(dispatch) {

  const creators = Map()
  .merge(...actions)
  .filter(value => typeof value === 'function')
  .toObject();

  return {
    actions: bindActionCreators(creators, dispatch),
    dispatch
  };
}
class Result extends React.Component {
  constructor(props) {
    super(props);
  }


  render(){
    return (
      <View style={styles.container}>
      <Text style={styles.question}>Result</Text>
      </View>
    );
  }
}

Result.propTypes = { }

const styles = StyleSheet.create({
  container: {
    justifyContent: "center",
    flex: 1
  },
  activityIndicator: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  header: {
    height: 60,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#3F51B5',
    flexDirection: 'column',
    paddingTop: 25
  },
  headerText: {
    fontWeight: 'bold',
    fontSize: 20,
    color: 'white'
  },
  text: {
    color: 'white',
    paddingHorizontal: 8,
    fontSize: 16
  },
  rowStyle: {
    paddingVertical: 20,
    paddingLeft: 16,
    borderTopColor: 'white',
    borderLeftColor: 'white',
    borderRightColor: 'white',
    borderBottomColor: '#E0E0E0',
    borderWidth: 1
  },
  rowText: {
    color: '#212121',
    fontSize: 16
  },
  subText: {
    fontSize: 14,
    color: '#757575'
  },
  section: {
    flexDirection: 'column',
    justifyContent: 'center',
    // alignItems: 'flex-start',
    padding: 6,
    backgroundColor: '#2196F3'
  },
  question: {
    borderWidth: 2,
    borderColor: '#00E3E3',
    padding: 2,
    borderRadius: 5,
    fontSize: 12,
    backgroundColor: '#FFFFFF',
    color: '#474747'
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(Result);
