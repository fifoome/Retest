/**
* # FormButton.js
*
* Display a button that responds to onPress and is colored appropriately
*/
'use strict';
/**
 * ## Imports
 *
 * React
 */
 import React from 'react';
 import {
   StyleSheet,
   View
 } from 'react-native';

/**
 * The platform neutral button
 */
const  Button = require('apsl-react-native-button');

/**
 * ## Styles
 */
var styles = StyleSheet.create({
  signin: {
    marginLeft: 10,
    marginRight: 10
  },
  textStyle: {
    color: 'white'
  },
  button: {
    backgroundColor: '#003399',
    borderColor:  '#ccffff',
    marginLeft: 10,
    marginTop: 10,
    marginRight: 10
  }

});

var FormButton = React.createClass({
  /**
   * ### render
   *
   * Display the Button
   */
  render() {
    return (
      <View style={styles.signin}>
        <Button style={styles.button}
            textStyle={styles.textStyle}
            isDisabled={this.props.isDisabled}
            onPress={this.props.onPress}
        >
          {this.props.buttonText}
        </Button>
      </View>
    );
  }
});

module.exports = FormButton;
