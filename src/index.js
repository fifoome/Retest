// 'use strict'
import { Component, View, Text, StyleSheet } from 'react-native';
import React from 'react';

import configureStore from './store/configureStore'
import globalInitialState from './reducers/global/globalInitialState';
import testInitialState from './reducers/test/testInitialState';
import formationInitialState from './reducers/formation/formationInitialState';
import deductionInitialState from './reducers/deduction/deductionInitialState';
import profileInitialState from './reducers/profile/profileInitialState';

import { setStore } from './reducers/global/globalActions';

import RNRF, { Router, Scene, TabBar} from 'react-native-router-flux';
import { Provider, connect } from 'react-redux';

import Entry from './containers/home/Entry';
import Test from './containers/test/Test';
import Testa from './containers/test/Testa';
import Deduction from './containers/deduction/Deduction';
import Testb from './containers/test/Testb';
import McpScreen from './containers/formation/McpScreen';
import IndexFormation from './containers/formation/IndexFormation';

import GoogleAnalytics from 'react-native-google-analytics-bridge';

//get initial state
function getInitialState() {
  const _initState = {
    global: (new globalInitialState),
    test: (new testInitialState),
    formation: (new formationInitialState),
    deduction: new deductionInitialState,
    profile: new profileInitialState
  };
  return _initState;
}

export default class Main extends React.Component {
  render() {

    GoogleAnalytics.setTrackerId('UA-82766021-1');
    GoogleAnalytics.setDryRun(true);
    const store = configureStore(getInitialState());
    store.dispatch(setStore(store));

    let test = function() {
      console.info("test");
    };
    // <Router hideNavBar={true} navigationBarStyle={styles.navBar} titleStyle={styles.navBarTitle} barButtonTextStyle={styles.barButtonTextStyle} barButtonIconStyle={styles.barButtonIconStyle} getSceneStyle={getSceneStyle}>

    return (
      <Provider store={store}>
      	  <Router hideNavBar={true} getSceneStyle={getSceneStyle}>
          <Scene key="root" onLeft={()=>test()} leftTitle="Lefdst">
              <Scene key="deduction" component={Deduction} title="Mode Deduction" onLeft={()=>test()} leftTitle="Left"/>
              <Scene key="entry" component={Entry} title="Entry" initial={true} />

              <Scene key="test" component={Test} title="Test"/>
              <Scene key="testa" component={Testa} title="Testa"/>
              <Scene key="testb" component={Testb} title="Testb"/>
              <Scene key="indexFormation" component={IndexFormation} title="Formation" onLeft={()=>test()} leftTitle="Lefdst"/>
              <Scene key="mcpScreen" component={McpScreen} title="McpScreen"/>
              </Scene>
          </Router>
      </Provider>
    )
  }
}

const getSceneStyle = (/* NavigationSceneRendererProps */ props, computedProps) => {
  const style = {
    flex: 1,
    backgroundColor: '#fff',
    shadowColor: null,
    shadowOffset: null,
    shadowOpacity: null,
    shadowRadius: null,
  };
  if (computedProps.isActive) {
    style.marginTop = computedProps.hideNavBar ? 0 : 64;
    style.marginBottom = computedProps.hideTabBar ? 0 : 50;
  }
  return style;
};

const styles = StyleSheet.create({
  navBar: {
      backgroundColor:'#0D47A1',
  },
  navBarTitle:{
      color:'#FFFFFF'
  },
  barButtonTextStyle:{
      color:'#FFFFFF'
  },
  barButtonIconStyle:{
      tintColor:'rgb(255,255,255)'
  }
});
