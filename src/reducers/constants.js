import keyMirror from 'key-mirror';

export default keyMirror({
  SET_STATE: null,
  GET_STATE: null,
  SET_STORE: null,

  START_TEST: null,

  SWITCH_FORMATION: null,

  NEXT_NODE: null,
  INIT_DEDUCTION: null,
  POP_QUESTION: null,

  ON_PROFILE_FORM_FIELD_CHANGE: null,
  GET_PROFILE_REQUEST: null,
  GET_PROFILE_SUCCESS: null,
  GET_PROFILE_FAILURE: null,

  PROFILE_UPDATE_REQUEST: null,
  PROFILE_UPDATE_SUCCESS: null,
  PROFILE_UPDATE_FAILURE: null,

  LOGOUT_SUCCESS: null
});
