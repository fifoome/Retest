import {Actions} from 'react-native-router-flux';

const {
  NEXT_NODE,
  INIT_DEDUCTION,
  POP_QUESTION
} = require('../constants').default;


// first called
export function deductionScreen() {
    return dispatch => { dispatch(initDeduction()); Actions.deduction() };
}

// called to change question
export function questionScreen(state) {
    return dispatch => { dispatch(nextNode(state)); Actions.deduction() };
}


// export function resultScreen(state) {
//
//     return dispatch => { dispatch(nextNode(state))};
//     // return dispatch => { Actions.testa()};
// }

export function nextNode(state) {
  return {
    type: NEXT_NODE,
    payload: state
  };
}

export function initDeduction() {
  return {
    type: INIT_DEDUCTION,
    payload: null
  };
}

export function popQuestion() {
    console.info("popQuestion");
  return {
    type: POP_QUESTION,
    payload: null
  };
}
