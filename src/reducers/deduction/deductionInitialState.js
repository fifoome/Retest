'use strict';

const {Record, List} = require('immutable');

const Node = Record({
	currentNode: 0,
    historyNodes: (List()),
	nodesList: new (Record({variablesList:(List()), productList:(List())}))
});

var InitialState = Record({
  node: new Node
});

export default InitialState;
