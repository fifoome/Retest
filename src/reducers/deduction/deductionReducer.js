'use strict';

const {
  NEXT_NODE,
  INIT_DEDUCTION,
  POP_QUESTION
} = require('../constants').default;

import InitialState from './deductionInitialState';

import { getVariable, getNextNode, doCondition } from '../../modules/engine';

const initialState = new InitialState;

export default function deductionReducer(state = initialState, action) {
  if (!(state instanceof InitialState)) return initialState.merge(state);

  switch (action.type) {
  case NEXT_NODE:
    var data = require('../../ressources/JSON/file.json');
    console.info("in next node");
    console.info(state.node.nodesList);
    console.info(state.node.historyNodes);
    // step 1: check condition and find conditionID
    var condLength = data.node[action.payload.nodeID]
        .completions.completion[action.payload.answerID]
        .condition.length;
    var conditionID = -1;
    // console.info("before for: "  + condLength);
    for (var potentialID = 0; potentialID < condLength; potentialID++) {
        // console.info("before doCondition: " + potentialID + " " + condLength);
        if (doCondition(data, action.payload.nodeID, action.payload.answerID, i, state.node.nodesList.variablesList) == true)
        {
            // console.info("potentialID = " + potentialID);
            conditionID = potentialID;
            break;
        }
    }
    if (conditionID == -1)
    {
        console.warn("Error in deductionReducer/NEXT_NODE, conditionID not found, set at 0");
        conditionID = 0;
    }
    console.info("conditionID: " + conditionID);

    //step 2
    var next = getNextNode(data, action.payload.nodeID, action.payload.answerID, 100);

    if (next == -2)
        return state.setIn(['node', 'currentNode'], next);
    else {
        // -----
        // do return
        // step 1: record old state
        // step 2: set productList
        // step 3: set variablesList
        // step 4: set currentNode
        return state.setIn(['node', 'historyNodes'],
        state.node.historyNodes.push({currentNode: state.node.currentNode, nodesList:state.node.nodesList}))
        .setIn(['node', 'currentNode'], next);
        // return state.setIn(['node', 'nodes'], state.node.nodes.push(action.payload)).setIn(['node', 'currentNode'], execAnswer(action.payload.nodeID, action.payload.answerID));
        // return state.setIn(['node', 'nodes'], state.node.nodes.push(action.payload))
        // .setIn(['node', 'currentNode'], execAnswer(action.payload.nodeID, action.payload.answerID));
    }

  case INIT_DEDUCTION:
    console.info("in init deduction");
    var data = require('../../ressources/JSON/file.json');
    console.info(state.node.nodesList);
    return state.setIn(['node', 'currentNode'], 0)
    .setIn(['node', 'nodesList', 'variablesList'], getVariable(data))
    .setIn(['node', 'nodesList', 'productList'], state.node.nodesList.productList.push("test"));
    // return state.setIn(['node', 'currentNode'], 0).setIn(['node', 'currentNode'], 0);

  case POP_QUESTION:
    console.info("in pop");
    return state;
  }

  return state;
}
