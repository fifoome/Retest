
// 'use strict';

import global from './global/globalReducer';
import test from './test/testReducer';
import formation from './formation/formationReducer';
import deduction from './deduction/deductionReducer';
import profile from './profile/profileReducer';

import { combineReducers } from 'redux';

const rootReducer = combineReducers({
  global,
  test,
  formation,
  deduction,
  profile
});

export default rootReducer;
