/**
 * # globalReducer.js
 *
 *
 */
'use strict';
/**
 * ## Imports
 * The InitialState for auth
 * fieldValidation for validating the fields
 * formValidation for setting the form's valid flag
 */
const {
  GET_STATE,
  SET_STATE,
  SET_STORE

} = require('../constants').default;

import InitialState from './globalInitialState';

const initialState = new InitialState;
/**
 * ## globalReducer function
 * @param {Object} state - initialState
 * @param {Object} action - type and payload
 */
export default function globalReducer(state = initialState, action) {
  if (!(state instanceof InitialState)) return initialState.merge(state);

  switch (action.type) {

  case SET_STORE:
    return state.set('store',action.payload);

    /**
     * ### Get the current state from the store
     *
     * The Redux ```store``` provides the state object.
     * We convert each key to JSON and set it in the state
     *
     * *Note*: the global state removes the ```store```, otherwise,
     * when trying to convert to JSON, it will be recursive and fail
     */
  case GET_STATE:
    let _state = state.store.getState();

    if (action.payload) {
      let newState = {};
      return state.set('showState',action.payload)
        .set('currentState',newState);
    } else {
      return state.set('showState',action.payload);
    }

    /**
     * ### Set the state
     *
     * This is in support of Hot Loading
     *
     */
  case SET_STATE:
    debugger;
    var global = JSON.parse(action.payload).global;
    var next = state.set('showState', false)
          .set('currentState', null);
    return next;

  }

  return state;
}
