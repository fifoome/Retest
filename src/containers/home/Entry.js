import React from 'react';
import {
  AppRegistry,
  Image,
  ListView,
  TouchableHighlight,
  StyleSheet,
  RecyclerViewBackedScrollView,
  TouchableOpacity,
  AlertIOS,
  ActivityIndicatorIOS,
  Text,
  View
} from 'react-native';
import Button from "react-native-button";

import * as globalActions from '../../reducers/global/globalActions';
import * as formationActions from '../../reducers/formation/formationActions';
import * as deductionActions from '../../reducers/deduction/deductionActions';
import * as profileActions from '../../reducers/profile/profileActions';

import { Map } from 'immutable';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import { getQuestion, getAnswers } from '../../modules/engine';
import { Actions } from 'react-native-router-flux';

import NavigationBar from 'react-native-navbar';

import Home from '../../components/home/Home';
import Auth from '../../components/home/Auth';

const actions = [ globalActions, formationActions, deductionActions, profileActions ];

function mapStateToProps(state) {
  return {
    ...state
  };
};

function mapDispatchToProps(dispatch) {

  const creators = Map()
  .merge(...actions)
  .filter(value => typeof value === 'function')
  .toObject();

  return {
    actions: bindActionCreators(creators, dispatch),
    dispatch
  };
}
class Entry extends React.Component {
  constructor(props) {
    super(props);
    this.state = { auth : false };
  }
  render(){
    if (this.state.auth == true) {
      return (
        <View style={styles.container}>
          <Home />
        </View>
      );
    }
    else {
        return (
          <View style={styles.container}>
            <Auth />
          </View> );
    }
  }
}
var styles = StyleSheet.create({
  container: {
    flexDirection: 'column',
    flex: 1
  },
  summary: {
    fontFamily: 'BodoniSvtyTwoITCTT-Book',
    fontSize: 18,
    fontWeight: 'bold'
  },
  button: {
    backgroundColor: '#FF3366',
    borderColor:  '#FF3366',
    marginLeft: 10,
    marginRight: 10
  }
});
export default connect(mapStateToProps, mapDispatchToProps)(Entry);
