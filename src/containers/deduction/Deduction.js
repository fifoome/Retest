import React from 'react';
import {
  AppRegistry,
  Image,
  ListView,
  TouchableHighlight,
  StyleSheet,
  RecyclerViewBackedScrollView,
  TouchableOpacity,
  AlertIOS,
  ActivityIndicatorIOS,
  Text,
  View
} from 'react-native';
import Button from "react-native-button";

import * as globalActions from '../../reducers/global/globalActions';
import * as formationActions from '../../reducers/formation/formationActions';
import * as deductionActions from '../../reducers/deduction/deductionActions';

import { Map } from 'immutable';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import { getQuestion, getAnswers } from '../../modules/engine';
import {Actions} from 'react-native-router-flux';

import NavigationBar from 'react-native-navbar';

import Question from '../../components/deduction/Question';
import Result from '../../components/deduction/Result';

const actions = [ globalActions, formationActions, deductionActions ];

function mapStateToProps(state) {
  return {
    ...state
  };
};

function mapDispatchToProps(dispatch) {

  const creators = Map()
  .merge(...actions)
  .filter(value => typeof value === 'function')
  .toObject();

  return {
    actions: bindActionCreators(creators, dispatch),
    dispatch
  };
}
class Deduction extends React.Component {
  constructor(props) {
    super(props);
    this.state = { result : false, data: 'default' };
  }
  onChange = (data) => {
      this.setState({ data });
  }
  render(){
    console.info("deduc: " + this.props.deduction.node.currentNode);
    const { data } = this.state;
    node = this.props.deduction.node.currentNode;// put in state?
    var leftButtonConfig = {
      title: 'Back',
      handler: Actions.pop
    };

    if (node == -1) {
      return (
        <View style={styles.container}>
          <NavigationBar title={{title: this.state.data}} leftButton={ leftButtonConfig } />
          <Result nodeID={node} data={data} onChange={this.onChange} />
        </View>
      );
    }
    else if (node == -2) {
        return (
          <View style={styles.container}>
            <NavigationBar title={{title: "error: -2"}} leftButton={ leftButtonConfig }/>
          </View> );
    } else
    {
      return (
        <View style={styles.container}>
          <NavigationBar title={{title: this.state.data}} leftButton={ leftButtonConfig }/>
          <Question nodeID={node} data={data} onChange={this.onChange} />
        </View> );
    }
  }
}
var styles = StyleSheet.create({
  container: {
    flexDirection: 'column',
    flex: 1
  },
  summary: {
    fontFamily: 'BodoniSvtyTwoITCTT-Book',
    fontSize: 18,
    fontWeight: 'bold'
  },
  button: {
    backgroundColor: '#FF3366',
    borderColor:  '#FF3366',
    marginLeft: 10,
    marginRight: 10
  }
});
export default connect(mapStateToProps, mapDispatchToProps)(Deduction);
