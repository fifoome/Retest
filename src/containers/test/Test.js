import React from 'react';
import {
  AppRegistry,
  Image,
  ListView,
  TouchableHighlight,
  StyleSheet,
  RecyclerViewBackedScrollView,
  TouchableOpacity,
  AlertIOS,
  ActivityIndicatorIOS,
  Text,
  View
} from 'react-native';
import Button from "react-native-button";

import * as globalActions from '../../reducers/global/globalActions';
import * as formationActions from '../../reducers/formation/formationActions';
import * as deductionActions from '../../reducers/deduction/deductionActions';
const actions = [ globalActions, formationActions, deductionActions ];

import { Map } from 'immutable';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import { getQuestion, getAnswers } from '../../modules/engine';
import {Actions} from 'react-native-router-flux';



function mapStateToProps(state) {
  return {
    ...state
  };
};

function mapDispatchToProps(dispatch) {

  const creators = Map()
  .merge(...actions)
  .filter(value => typeof value === 'function')
  .toObject();

  return {
    actions: bindActionCreators(creators, dispatch),
    dispatch
  };
}
class Test extends React.Component {
  constructor(props) {
    super(props);
    var getRowData = (dataBlob, rowID) => {
      return dataBlob[rowID];
    }

 // this.state = this.getInit(this.props);
    console.info(this.props.deduction.node.currentNode);
    console.info("new screen");
    this.state = {
      loaded : false,
      nodeID : this.props.deduction.node.currentNode,
      dataSource : new ListView.DataSource({
        getRowData              : getRowData,
        rowHasChanged           : (row1, row2) => row1 !== row2
      })
    };
    // this.renderRow = this.renderRow.bind(this);
    this.bindMethods();


  }
  getInit(props) {

    var getRowData = (dataBlob, rowID) => {
      return dataBlob[rowID];
    }

    return {
      loaded : false,
      currentNode: props.deduction.node.currentNode,
      dataSource : new ListView.DataSource({
        getRowData              : getRowData,
        rowHasChanged           : (row1, row2) => row1 !== row2
      })
    }
  }
  bindMethods() {
    if (! this.bindableMethods) {
      return;
    }

    for (var methodName in this.bindableMethods) {
      this[methodName] = this.bindableMethods[methodName].bind(this);
    }
  }


  componentDidMount() {
    this.fetchData();
  }
  fetchData () {
    this.setState({
      dataSource : this.state.dataSource.cloneWithRows(getAnswers(this.state.nodeID)),
      // dataSource : this.state.dataSource.cloneWithRows(getAnswers(0)),
      loaded     : true
    });
  }
  // renderRow (rowData, sectionID, rowID) {
  //           console.info("fuckrender");
  //
  //     return (
  //       <TouchableOpacity onPress={() => buttonPressHandler.bind(null,
  //                                         0,0,
  //                                         rowID)}>
  //       <View style={styles.rowStyle}>
  //       <Text style={styles.rowText}> {rowData[rowID]} </Text>
  //       </View>
  //       </TouchableOpacity>
  //     );
  //   }

  render(){

    if (!this.state.loaded) {
      return this.renderLoadingView();
    }

    let renderRow = function (rowData, sectionID, rowID) {
        return (
          <TouchableOpacity onPress={() => Press(this.props, this.state.nodeID, rowID)}>
          <View style={styles.rowStyle}>
          <Text style={styles.rowText}> {rowData[rowID]} </Text>
          </View>
          </TouchableOpacity>
        );
    };
    renderRow = renderRow.bind(this);
    let Press = function (func, nodeID, answerID) {
      // func.actions.nextNode({id: nodeID, idAnswers:answerID});
      this.setState({
        nodeID:1,
        loaded     : true
      });
      // func.actions.refresh();
          // props.actions.resultScreen(0);
    };
    Press = Press.bind(this);
      //
    return (
      <View style={styles.container}>
      <Text style={styles.question}>Question #{this.state.nodeID}: { getQuestion(this.state.nodeID) } </Text>
      <ListView
      dataSource = {this.state.dataSource}
      style      = {styles.listview}
      renderRow  = {renderRow}
      />
      </View>
    );
  }
  renderLoadingView() {
    return (
      <View style={styles.header}>
      <Text style={styles.headerText}>Variables List</Text>
      <View style={styles.container}>
      <ActivityIndicatorIOS
      animating={!this.state.loaded}
      style={[styles.activityIndicator, {height: 80}]}
      size="large"
      />
      </View>
      </View>
    );
  }
}
Test.propTypes = {
       loaded: React.PropTypes.bool,
       dataSource: React.PropTypes.array }

const styles = StyleSheet.create({
  container: {
    justifyContent: "center",
    flex: 1
  },
  activityIndicator: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  header: {
    height: 60,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#3F51B5',
    flexDirection: 'column',
    paddingTop: 25
  },
  headerText: {
    fontWeight: 'bold',
    fontSize: 20,
    color: 'white'
  },
  text: {
    color: 'white',
    paddingHorizontal: 8,
    fontSize: 16
  },
  rowStyle: {
    paddingVertical: 20,
    paddingLeft: 16,
    borderTopColor: 'white',
    borderLeftColor: 'white',
    borderRightColor: 'white',
    borderBottomColor: '#E0E0E0',
    borderWidth: 1
  },
  rowText: {
    color: '#212121',
    fontSize: 16
  },
  subText: {
    fontSize: 14,
    color: '#757575'
  },
  section: {
    flexDirection: 'column',
    justifyContent: 'center',
    // alignItems: 'flex-start',
    padding: 6,
    backgroundColor: '#2196F3'
  },
  question: {
    borderWidth: 2,
    borderColor: '#00E3E3',
    padding: 2,
    borderRadius: 5,
    fontSize: 12,
    backgroundColor: '#FFFFFF',
    color: '#474747'
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(Test);
