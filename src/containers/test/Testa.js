import React from 'react';
import {View, Text, StyleSheet} from "react-native";
import Button from "react-native-button";

import * as globalActions from '../../reducers/global/globalActions';
import * as testActions from '../../reducers/test/testActions';

import {Map} from 'immutable';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

const actions = [ globalActions, testActions ];

function mapStateToProps(state) {
  return {
      ...state
  };
};

function mapDispatchToProps(dispatch) {

  const creators = Map()
          .merge(...actions)
          .filter(value => typeof value === 'function')
          .toObject();

  return {
    actions: bindActionCreators(creators, dispatch),
    dispatch
  };
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "transparent",
    // borderWidth: 2,
    // borderColor: 'red',
  }
});

class Testa extends React.Component {
// let Home = React.createClass({
  constructor(props) {
    super(props);
    // this.fu = props;
    // console.info(props.formation.id);
        // <Text>{this.props.deduction.products[0]}</Text>
  }
  render(){
    return (
      <View  style={styles.container}>
        <Text>Test page</Text>
        <Text>{this.props.deduction.node.currentNode}</Text>
      </View>
    );
  }
}
// });
// export default connect(mapStateToProps, mapDispatchToProps)(Home);
export default connect(mapStateToProps, mapDispatchToProps)(Testa);
