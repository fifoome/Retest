import React from 'react';
import { View, Text, StyleSheet, WebView } from "react-native";
import Button from "react-native-button";

import * as globalActions from '../../reducers/global/globalActions';
import * as formationActions from '../../reducers/formation/formationActions';

import { Map } from 'immutable';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
// import { HTMLView } from 'react-native-htmlview';

const actions = [ globalActions, formationActions ];
var BGWASH = 'rgba(255,255,255,0.8)';
var WEBVIEW_REF = 'webview';
function mapStateToProps(state) {
  return {
    ...state
  };
};

function mapDispatchToProps(dispatch) {

  const creators = Map()
  .merge(...actions)
  .filter(value => typeof value === 'function')
  .toObject();

  return {
    actions: bindActionCreators(creators, dispatch),
    dispatch
  };
}


const styles = StyleSheet.create({
  webView: {
      backgroundColor: BGWASH,
	      borderWidth: 2,
    borderColor: 'red'
    },
  container: {
    flex: 1,
	flexDirection: 'column',
    backgroundColor: "transparent",
    marginTop:  70,
    borderWidth: 2,
    borderColor: 'red'
  }
});

class McpScreen extends React.Component {
  constructor(props) {
    super(props);
    // html3 = require('./index.html');
  }
  render(){
  // var htmlCode = require('./index.html');
	var html2 = "<h1 style=\"text-align: center;\">Exemple pour L'Oreal</h1><p>&nbsp;</p><p>ulisation de bullet point</p><div class=\"boxed\"><div id=\"lipsum\"><ul><li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li><li>Vivamus quis nibh sit amet elit consectetur faucibus.</li><li>Cras nec erat posuere, aliquam odio eu, semper ante.</li><li>Morbi et ligula lobortis, euismod purus nec, auctor neque.</li><li>Pellentesque egestas elit eu erat condimentum congue.</li><li>Integer euismod sapien ut nulla vestibulum, eu vehicula ipsum commodo.</li></ul><p>&nbsp;</p><p><span style=\"color: #ff6600;\">Morbi</span> et <strong>ligula</strong> lobortis, euismod <em>purus</em> nec, auctor neque.</p></div></div>";
	var html4 = "<h1>afsdfasdfasd</h1><p>&nbsp;</p><p><img src=\"Picture1\" alt=\"Smiley face\" height=\"420\" width=\"420\"/></p>"

    return (

      <View style={styles.container}>
        <WebView style={styles.webView} source={ require('../../ressources/index.html') } javaScriptEnabled={true} automaticallyAdjustContentInsets={false} decelerationRate="normal" />
      </View>
    );

}
}
export default connect(mapStateToProps, mapDispatchToProps)(McpScreen);
