import React from 'react';
import { View, Text, StyleSheet } from "react-native";
import Button from "react-native-button";

import * as globalActions from '../../reducers/global/globalActions';
import * as testActions from '../../reducers/test/testActions';
import * as formationActions from '../../reducers/formation/formationActions';

import { Map } from 'immutable';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

const actions = [ globalActions, testActions, formationActions ];

function mapStateToProps(state) {
  return {
    ...state
  };
};

function mapDispatchToProps(dispatch) {

  const creators = Map()
  .merge(...actions)
  .filter(value => typeof value === 'function')
  .toObject();

  return {
    actions: bindActionCreators(creators, dispatch),
    dispatch
  };
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "transparent",
    // borderWidth: 2,
    // borderColor: 'red',
  }
});

class IndexFormation extends React.Component {
  // let IndexFormation = React.createClass({
  constructor(props) {
    super(props);
    // this.fu = props;
    // console.info(props.formation.id);

  }

  render(){

    let profileButtonText = 'Go to test pagegf';
    let past_matches_circles =[];
    // let renderarray = () => {
    let renderarray = function(props) {

      for(var i=0; i<10; i++){
        past_matches_circles.push(
          renderbutton(i, props)
        );
      }
    };
    let press = function(id, props) {//todo cleaner
      props.actions.mcpScreen();
    };
    var renderbutton = function(i, props) {
      return <Button key={i} onPress={() => press(i, props)}>Test view html {i}</Button>;
    };
    this.props.actions.getState()
    // console.info("here1");
    // console.info(this.props);
    // console.info(this.state);
    renderarray(this.props);
    return (
      <View style={styles.container}>
      <Text>Formation page: {this.props.formation.id}</Text>
      {past_matches_circles}
      </View>
    );
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(IndexFormation);
